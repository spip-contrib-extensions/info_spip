# Info SPIP

![info_spip](./prive/themes/spip/images/info_spip-xx.svg)

Ce plugin permet d’avoir une page, dans la partie privée, donnant la fiche identitaire de votre site et de votre serveur.
Il offre aussi la possiblité d’avoir une page au format xml (dans la partie publique) reprenant les même informations que la page du privé.

## Documentation

https://contrib.spip.net/4546
